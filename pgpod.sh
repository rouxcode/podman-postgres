#!/bin/bash

start_pod() {
  # Check if a version argument is provided
  if [ -z "$1" ]; 
  then
    echo "Usage: $0 <postgres_version>"
    exit 1
  fi

  VERSION=$1

  # Validate that the version is an integer and greater than or equal to 10
  if ! [[ "$VERSION" =~ ^[0-9]+$ ]] || [ "$VERSION" -lt 10 ]; 
  then
    echo "Only postgres >= 10"
    exit 1
  fi

  # Check if XDG_CONFIG_HOME is set and if it is a directory
  XDG_DIR="${XDG_CONFIG_HOME:-$HOME/.config}"
  if [ -z "$XDG_DIR" ] || [ ! -d "$XDG_DIR" ]; 
  then
    echo "Error: XDG_CONFIG_HOME is not set or not a directory."
    exit 1
  fi

  # check if a config for the given version exists
  CONF_DIR="$XDG_DIR/pgpod"
  mkdir -p $CONF_DIR

  # Check if the configuration file for the given version exists else create one
  CONF_FILE="$CONF_DIR/$VERSION.conf"
  if [ ! -f "$CONF_FILE" ]; 
  then
    echo "Error: Conf file does not exist"
    echo "Creating $CONF_FILE"
    touch "${CONF_FILE}"
    chmod 600 "${CONF_FILE}"
  fi

  # Check if the required variables are in the configuration file
  if ! grep -q "POSTGRES_USER" "$CONF_FILE" || ! grep -q "POSTGRES_PASSWORD" "$CONF_FILE"; 
  then
    echo "POSTGRES_USER or POSTGRES_PASSWORD not found"
    echo "adding default POSTGRES_USER and POSTGRES_PASSWORD"
    echo -e "POSTGRES_USER=postgres\nPOSTGRES_PASSWORD=$(openssl rand -base64 12)" >>"$CONF_FILE"
  fi

  CONTAINER_NAME="postgres$VERSION"
  VOLUME_NAME="postgres$VERSION"
  EXTERNAL_PORT=$((5400 + VERSION))
  INTERNAL_PORT=5432
  PGDATA='/var/lib/postgresql/data/pg'

  # Create the volume if it doesn't exist
  podman volume inspect $VOLUME_NAME &>/dev/null
  if [ $? -ne 0 ]; 
  then
    podman volume create $VOLUME_NAME
  fi

  # Run the PostgreSQL container
  podman run -d \
    --name $CONTAINER_NAME \
    --env-file "$CONF_FILE" \
    -e PGDATA="${PGDATA}" \
    -p $EXTERNAL_PORT:$INTERNAL_PORT \
    --user 1000:1000 \
    -v $VOLUME_NAME:/var/lib/postgresql/data \
    --pull always \
    postgres:${VERSION}-bookworm

  echo "$CONTAINER_NAME - started successfully."
  exit 0
}

stop_pod() {
  # Check if a version argument is provided
  if [ -z "$1" ]; then
    echo "Usage: $0 down <postgres_version>"
    exit 1
  fi

  VERSION=$1

  # Validate that the version is an integer and greater than or equal to 10
  if ! [[ "$VERSION" =~ ^[0-9]+$ ]] || [ "$VERSION" -lt 10 ]; then
    echo "Only postgres >= 10"
    exit 1
  fi

  CONTAINER_NAME="postgres$VERSION"

  # Stop and remove the container
  podman stop $CONTAINER_NAME
  podman rm $CONTAINER_NAME

  echo "$CONTAINER_NAME - stopped and removed successfully."
  exit 0
}

# Main script logic
if [ "$1" == "up" ]; then
  shift
  start_pod "$@"
elif [ "$1" == "down" ]; then
  shift
  stop_pod "$@"
else
  echo "Usage: $0 {start|down} <postgres_version>"
  exit 1
fi